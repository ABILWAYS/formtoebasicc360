﻿using System.Collections.Generic;
using FormCsvToJson.Models.New;
using Newtonsoft.Json;

namespace FormCsvToJson
{
    public class Converter
    {
        public Form ToJsonForm(string payload)
        {
            Dictionary<string, object> dict = JsonConvert.DeserializeObject<Dictionary<string, object>>(JsonConvert.DeserializeObject<Dictionary<string, object>>(payload)["data"].ToString());
            Form form = new Form();
            Models.Old.Form oldForm = JsonConvert.DeserializeObject<Models.Old.Form>(payload);

            // Pas d'envoi de formulaire brouillon
            if (oldForm.InDraft == "1")
                return null;

            #region Mapping common old to new
            form.Serial = oldForm.Serial;
            form.Sid = oldForm.Sid;
            form.Uuid = oldForm.Uuid;
            form.Token = "";
            form.Uri = oldForm.Uri;
            form.Created = oldForm.Created;
            form.Completed = oldForm.Completed;
            form.InDraft = oldForm.InDraft == "1";
            form.CurrentPage = oldForm.CurrentPage;
            form.RemoteAddr = oldForm.RemoteAddr;
            form.Uid = oldForm.Uid;
            form.Langcode = oldForm.Langcode;
            form.WebformId = oldForm.WebformId;
            form.Notes = oldForm.Notes;
            form.Source = oldForm.DomainId;
            form.Bu = oldForm.DomainId.ToUpper();
            // Data
            form.Data = new Data
            {
                TermsAndConditionsCheckbox = oldForm.Data.ApplicationTermsAndConditionsCheckbox == "1",
                TrainingCouponCode = oldForm.Data.ApplicationTrainingCouponCode,
                UserProfile = oldForm.Data.ApplicationUserProfile,
                UserSubProfile = oldForm.Data.ApplicationUserSubProfile,
                BillingAddress = oldForm.Data.BillingAddress,
                Optin = oldForm.Data.Optin == "1",
                Price = oldForm.Data.Price,
                OtherAddress = new Address
                {
                    City = oldForm.Data.OtherCity,
                    Country = oldForm.Data.OtherCountry,
                    Mailbox = oldForm.Data.OtherMailbox,
                    Postcode = oldForm.Data.OtherPostcode,
                    Street = oldForm.Data.OtherAddress,
                    Street2 = oldForm.Data.OtherAddressComplement
                }
            };
            form.Data.Price = oldForm.Data.Price;
            form.Data.Product = oldForm.Data.Product;
            #endregion

            #region Mapping company old to new
            if (oldForm.Data.ApplicationUserProfile != "private_buyer")
            {
                form.Data.Company = new Company
                {
                    Address = new Address
                    {
                        City = oldForm.Data.CompanyAddress.City,
                        Country = oldForm.Data.CompanyAddress.Country,
                        Postcode = oldForm.Data.CompanyAddress.PostalCode,
                        StateProvince = oldForm.Data.CompanyAddress.StateProvince,
                        Street = oldForm.Data.CompanyAddress.Address1,
                        Street2 = oldForm.Data.CompanyAddress.Address2
                    },
                    Name = oldForm.Data.ApplicationCompanyName,
                    Number = oldForm.Data.ApplicationCompanyNumber
                };
            }
            #endregion

            #region Mapping Responsible old to new
            form.Data.Responsible = new Person
            {
                EmailAddress = oldForm.Data.ApplicationResponsibleEmailAddress,
                FirstName = oldForm.Data.ApplicationResponsibleFirstName,
                LastName = oldForm.Data.ApplicationResponsibleLastName,
                Phone = oldForm.Data.ApplicationResponsiblePhoneNumber,
                Position = oldForm.Data.ApplicationResponsiblePosition,
                Title = oldForm.Data.ApplicationResponsibleTitle
            };
            #endregion

            #region Mapping Sponsor old to new
            form.Data.Sponsor = new Person
            {
                EmailAddress = oldForm.Data.Email,
                FirstName = oldForm.Data.FirstName,
                LastName = oldForm.Data.LastName,
                Phone = oldForm.Data.ApplicationResponsiblePhoneNumber,
                Position = oldForm.Data.ParticipantPosition,
                Title = oldForm.Data.Civility,
            };
            #endregion

            #region Mapping opca old to new
            form.Data.Opca = new Opca
            {
                Address = new Address
                {
                    City = oldForm.Data.OpcaCity,
                    Country = oldForm.Data.OpcaCountry,
                    Mailbox = oldForm.Data.OpcaMailbox,
                    Postcode = oldForm.Data.OpcaPostCode,
                    Street = oldForm.Data.OpcaAddress,
                    Street2 = oldForm.Data.OpcaComplementAddress
                },
                Name = oldForm.Data.OpcaName,
                Used = oldForm.Data.Opca == "yes"
            };
            #endregion

            #region Mapping trainees old to new

            if (form.Data.UserProfile == "particulier" || form.Data.UserProfile == "independant")
            {
                form.Data.Trainees.Add(new Trainee
                {
                    Address = new Address
                    {
                        City = oldForm.Data.ParticipantCity,
                        Country = oldForm.Data.ParticipantCountry,
                        Street = oldForm.Data.ParticipantAddress,
                        Street2 = oldForm.Data.ParticipantComplementAddress,
                        Postcode = oldForm.Data.ParticipantPostCode
                    },
                    Person = new Person
                    {
                        EmailAddress = oldForm.Data.ParticipantEmail,
                        FirstName = oldForm.Data.ParticipantFirstName,
                        LastName = oldForm.Data.ParticipantLastName,
                        Phone = oldForm.Data.ParticipantPhone,
                        Position = oldForm.Data.ParticipantPosition,
                        Title = oldForm.Data.ParticipantCivility
                    },
                    Session = oldForm.Data.Session
                });
            }
            else
            {
                for (int idx = 1; idx < 20; idx++)
                {
                    if (!dict.TryGetValue($"application_trainee_{idx}", out object val))
                        break;
                    Models.Old.Trainee oldTrainee = JsonConvert.DeserializeObject<Models.Old.Trainee>(val.ToString());
                    form.Data.Trainees.Add(new Trainee
                    {
                        Used = oldTrainee.IsUsed == "1",
                        Manager = new Person
                        {
                            EmailAddress = oldTrainee.ManagerEmailAddress,
                            FirstName = oldTrainee.ManagerFirstName,
                            LastName = oldTrainee.ManagerLastName,
                            Title = oldTrainee.ManagerTitle,
                        },
                        Options = new Options
                        {
                            Cvrex = oldTrainee.OptionsCvrex,
                            CvrexSession = oldTrainee.Session,
                            End = oldTrainee.ManagerEnd,
                            Start = oldTrainee.ManagerStart,
                        },
                        Person = new Person
                        {
                            EmailAddress = oldTrainee.EmailAddress,
                            FirstName = oldTrainee.FirstName,
                            LastName = oldTrainee.LastName,
                            Phone = oldTrainee.PhoneNumber,
                            Position = oldTrainee.Position,
                            Title = oldTrainee.TraineeTitle
                        },
                        Session = oldTrainee.Session
                    });
                }
            }
            #endregion

            return form;
        }
    }
}