﻿using System;
using System.IO;
using Renci.SshNet;
using Renci.SshNet.Common;

namespace FormCsvToJson
{
    public class SftpConnector
    {
        #region PROPERTIES
        private string Host;

        private int Port = 22;

        private string Username { get; set; }

        private string Password { get; set; }

        private SftpClient Client { get; set; }
        #endregion

        public SftpConnector(string hostIP, string userName, string password, int port = 22)
        {
            Host = hostIP;
            Username = userName;
            Password = password;
            Port = port;
            var connectionKey = new KeyboardInteractiveConnectionInfo(Host, Port, Username);
            var connectionPasw = new PasswordAuthenticationMethod(Username, Password);
            var connectionInfo = new ConnectionInfo(Host, Port, Username, connectionPasw);

            connectionKey.AuthenticationPrompt += delegate (object sender, AuthenticationPromptEventArgs e)
            {
                foreach (var prompt in e.Prompts)
                {
                    if (prompt.Request.Equals("Password: ", StringComparison.InvariantCultureIgnoreCase))
                    {
                        prompt.Response = Password;
                    }
                }
            };
            Client = new SftpClient(connectionInfo);
        }

        public void DownloadDirectory(string source, string destination, string extension)
        {
            if (!Client.IsConnected)
                Client.Connect();
            var files = Client.ListDirectory(source);
            foreach (var file in files)
            {
                if (!file.IsDirectory && !file.IsSymbolicLink && file.Name.EndsWith(extension))
                {
                    DownloadFile(file.FullName, destination);
                }
                else if (file.IsSymbolicLink)
                {
                    Console.WriteLine("Ignoring symbolic link {0}", file.FullName);
                }
            }
        }

        public void UploadFile(string workingdirectory, string uploadfile)
        {
            if (!Client.IsConnected)
                Client.Connect();
            Console.WriteLine("Connected to {0}", Host);
            Client.ChangeDirectory(workingdirectory);
            Console.WriteLine("Changed directory to {0}", workingdirectory);
            try
            {
                using (var fileStream = new FileStream(uploadfile, FileMode.Open))
                {
                    Console.WriteLine("Uploading {0} ({1:N0} bytes)", uploadfile, fileStream.Length);
                    Client.BufferSize = 4 * 1024;
                    Client.UploadFile(fileStream, Path.GetFileName(uploadfile));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Une erreur est survenue lors de l'upload du fichier: {0} :{1}{2}", uploadfile, Environment.NewLine, e.Message);
            }
        }

        public void DownloadFile(string path, string directory)
        {

            if (!Client.IsConnected)
                Client.Connect();
            string filename = path.Substring(path.LastIndexOf('/') + 1);
            Console.WriteLine("Downloading {0}", path);
            using (Stream fileStream = File.OpenWrite(Path.Combine(directory, filename)))
            {
                Client.DownloadFile(path, fileStream);
            }
        }
    }
}