﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FormCsvToJson.Models
{
    [DataContract]
    public class Email
    {
        [DataMember(Name = "receiver")]
        public string Receiver { get; set; }

        [DataMember(Name = "sender")]
        public string Sender { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "message")]
        public string Message { get; set; }

        [DataMember(Name = "header")]
        public string Header { get; set; }

        [DataMember(Name = "attachments")]
        public List<string> Attachments { get; set; }

        [DataMember(Name = "links_only")]
        public bool LinksOnly { get; set; }
    }
}