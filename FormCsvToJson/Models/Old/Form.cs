﻿using System.Runtime.Serialization;

namespace FormCsvToJson.Models.Old
{
    [DataContract]
    public class Form
    {
        [DataMember(Name = "serial")]
        public string Serial { get; set; }

        [DataMember(Name = "sid")]
        public string Sid { get; set; }

        [DataMember(Name = "uuid")]
        public string Uuid { get; set; }

        [DataMember(Name = "uri")]
        public string Uri { get; set; }

        [DataMember(Name = "created")]
        public string Created { get; set; }

        [DataMember(Name = "completed")]
        public string Completed { get; set; }

        [DataMember(Name = "changed")]
        public string Changed { get; set; }

        [DataMember(Name = "in_draft")]
        public string InDraft { get; set; }

        [DataMember(Name = "current_page")]
        public string CurrentPage { get; set; }

        [DataMember(Name = "remote_addr")]
        public string RemoteAddr { get; set; }

        [DataMember(Name = "uid")]
        public string Uid { get; set; }

        [DataMember(Name = "langcode")]
        public string Langcode { get; set; }

        [DataMember(Name = "webform_id")]
        public string WebformId { get; set; }

        [DataMember(Name = "entity_type")]
        public string EntityType { get; set; }

        [DataMember(Name = "entity_id")]
        public string EntityId { get; set; }

        [DataMember(Name = "locked")]
        public string Locked { get; set; }

        [DataMember(Name = "sticky")]
        public string Sticky { get; set; }

        [DataMember(Name = "notes")]
        public string Notes { get; set; }

        [DataMember(Name = "domain_id")]
        public string DomainId { get; set; }

        [DataMember(Name = "data")]
        public Data Data { get; set; }
    }

    [DataContract]
    public class Data
    {
        [DataMember(Name = "application_company_name")]
        public string ApplicationCompanyName { get; set; }

        [DataMember(Name = "application_company_number")]
        public string ApplicationCompanyNumber { get; set; }

        [DataMember(Name = "application_responsible_email_address")]
        public string ApplicationResponsibleEmailAddress { get; set; }

        [DataMember(Name = "application_responsible_first_name")]
        public string ApplicationResponsibleFirstName { get; set; }

        [DataMember(Name = "application_responsible_last_name")]
        public string ApplicationResponsibleLastName { get; set; }

        [DataMember(Name = "application_responsible_phone_number")]
        public string ApplicationResponsiblePhoneNumber { get; set; }

        [DataMember(Name = "application_responsible_position")]
        public string ApplicationResponsiblePosition { get; set; }

        [DataMember(Name = "application_responsible_title")]
        public string ApplicationResponsibleTitle { get; set; }

        [DataMember(Name = "application_terms_and_conditions_checkbox")]
        public string ApplicationTermsAndConditionsCheckbox { get; set; }

        [DataMember(Name = "application_training_coupon_code")]
        public string ApplicationTrainingCouponCode { get; set; }

        [DataMember(Name = "application_user_profile")]
        public string ApplicationUserProfile { get; set; }

        [DataMember(Name = "application_user_sub_profile")]
        public string ApplicationUserSubProfile { get; set; }

        [DataMember(Name = "billing_address")]
        public string BillingAddress { get; set; }

        [DataMember(Name = "civility")]
        public string Civility { get; set; }

        [DataMember(Name = "company_address")]
        public Address CompanyAddress { get; set; }

        [DataMember(Name = "conference")]
        public string Conference { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "first_name")]
        public string FirstName { get; set; }

        [DataMember(Name = "last_name")]
        public string LastName { get; set; }

        [DataMember(Name = "opca")]
        public string Opca { get; set; }

        [DataMember(Name = "opca_address")]
        public string OpcaAddress { get; set; }

        [DataMember(Name = "opca_city")]
        public string OpcaCity { get; set; }

        [DataMember(Name = "opca_complement_address")]
        public string OpcaComplementAddress { get; set; }

        [DataMember(Name = "opca_country")]
        public string OpcaCountry { get; set; }

        [DataMember(Name = "opca_mailbox")]
        public string OpcaMailbox { get; set; }

        [DataMember(Name = "opca_name")]
        public string OpcaName { get; set; }

        [DataMember(Name = "opca_post_code")]
        public string OpcaPostCode { get; set; }

        [DataMember(Name = "optin")]
        public string Optin { get; set; }

        [DataMember(Name = "other_address")]
        public string OtherAddress { get; set; }

        [DataMember(Name = "other_address_complement")]
        public string OtherAddressComplement { get; set; }

        [DataMember(Name = "other_city")]
        public string OtherCity { get; set; }

        [DataMember(Name = "other_country")]
        public string OtherCountry { get; set; }

        [DataMember(Name = "other_mailbox")]
        public string OtherMailbox { get; set; }

        [DataMember(Name = "other_postcode")]
        public string OtherPostcode { get; set; }

        [DataMember(Name = "participant_address")]
        public string ParticipantAddress { get; set; }

        [DataMember(Name = "participant_city")]
        public string ParticipantCity { get; set; }

        [DataMember(Name = "participant_civility")]
        public string ParticipantCivility { get; set; }

        [DataMember(Name = "participant_complement_address")]
        public string ParticipantComplementAddress { get; set; }

        [DataMember(Name = "participant_country")]
        public string ParticipantCountry { get; set; }

        [DataMember(Name = "participant_email")]
        public string ParticipantEmail { get; set; }

        [DataMember(Name = "participant_first_name")]
        public string ParticipantFirstName { get; set; }

        [DataMember(Name = "participant_last_name")]
        public string ParticipantLastName { get; set; }

        [DataMember(Name = "participant_phone")]
        public string ParticipantPhone { get; set; }

        [DataMember(Name = "participant_position")]
        public string ParticipantPosition { get; set; }

        [DataMember(Name = "participant_post_code")]
        public string ParticipantPostCode { get; set; }

        [DataMember(Name = "price")]
        public string Price { get; set; }

        [DataMember(Name = "product")]
        public string Product { get; set; }

        [DataMember(Name = "session")]
        public string Session { get; set; }

        [DataMember(Name = "session_dates")]
        public string SessionDates { get; set; }
    }

    [DataContract]
    public class Trainee
    {
        [DataMember(Name = "email_address")]
        public string EmailAddress { get; set; }

        [DataMember(Name = "first_name")]
        public string FirstName { get; set; }

        [DataMember(Name = "is_used")]
        public string IsUsed { get; set; }

        [DataMember(Name = "last_name")]
        public string LastName { get; set; }

        [DataMember(Name = "manager")]
        public string Manager { get; set; }

        [DataMember(Name = "manager_email_address")]
        public string ManagerEmailAddress { get; set; }

        [DataMember(Name = "manager_end")]
        public string ManagerEnd { get; set; }

        [DataMember(Name = "manager_first_name")]
        public string ManagerFirstName { get; set; }

        [DataMember(Name = "manager_last_name")]
        public string ManagerLastName { get; set; }

        [DataMember(Name = "manager_start")]
        public string ManagerStart { get; set; }

        [DataMember(Name = "manager_title")]
        public string ManagerTitle { get; set; }

        [DataMember(Name = "options_cvrex")]
        public string OptionsCvrex { get; set; }

        [DataMember(Name = "participant_information_end")]
        public string ParticipantInformationEnd { get; set; }

        [DataMember(Name = "participant_information_start")]
        public string ParticipantInformationStart { get; set; }

        [DataMember(Name = "phone_number")]
        public string PhoneNumber { get; set; }

        [DataMember(Name = "position")]
        public string Position { get; set; }

        [DataMember(Name = "session")]
        public string Session { get; set; }

        [DataMember(Name = "trainee")]
        public string Trainee1 { get; set; }

        [DataMember(Name = "trainee_title")]
        public string TraineeTitle { get; set; }
    }

    [DataContract]
    public class Address
    {
        [DataMember(Name = "address")]
        public string Address1 { get; set; }

        [DataMember(Name = "address_2")]
        public string Address2 { get; set; }

        [DataMember(Name = "city")]
        public string City { get; set; }

        [DataMember(Name = "country")]
        public string Country { get; set; }

        [DataMember(Name = "postal_code")]
        public string PostalCode { get; set; }

        [DataMember(Name = "state_province")]
        public string StateProvince { get; set; }
    }
}