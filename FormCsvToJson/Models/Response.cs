﻿using System.Runtime.Serialization;

namespace FormCsvToJson.Models
{
    [DataContract]
    class Response
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
}
