﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace FormCsvToJson.Models.New
{
    [DataContract]
    public class Form
    {
        [DataMember(Name = "serial", EmitDefaultValue = false)]
        public string Serial { get; set; }

        [DataMember(Name = "bu", EmitDefaultValue = false)]
        public string Bu { get; set; }

        [DataMember(Name = "sid", EmitDefaultValue = false)]
        public string Sid { get; set; }

        [DataMember(Name = "uuid", EmitDefaultValue = false)]
        public string Uuid { get; set; }

        [DataMember(Name = "token", EmitDefaultValue = false)]
        public string Token { get; set; }

        [DataMember(Name = "uri", EmitDefaultValue = false)]
        public string Uri { get; set; }

        [DataMember(Name = "created", EmitDefaultValue = false)]
        public string Created { get; set; }

        [DataMember(Name = "completed", EmitDefaultValue = false)]
        public string Completed { get; set; }

        [DataMember(Name = "changed", EmitDefaultValue = false)]
        public string Changed { get; set; }

        [DataMember(Name = "in_draft", EmitDefaultValue = false)]
        public bool InDraft { get; set; }

        [DataMember(Name = "current_page", EmitDefaultValue = false)]
        public string CurrentPage { get; set; }

        [DataMember(Name = "remote_addr", EmitDefaultValue = false)]
        public string RemoteAddr { get; set; }

        [DataMember(Name = "uid", EmitDefaultValue = false)]
        public string Uid { get; set; }

        [DataMember(Name = "langcode", EmitDefaultValue = false)]
        public string Langcode { get; set; }

        [DataMember(Name = "webform_id", EmitDefaultValue = false)]
        public string WebformId { get; set; }

        [DataMember(Name = "notes", EmitDefaultValue = false)]
        public string Notes { get; set; }

        [DataMember(Name = "source", EmitDefaultValue = false)]
        public string Source { get; set; }

        [DataMember(Name = "data", EmitDefaultValue = false)]
        public Data Data { get; set; }

        public string Dump() => JsonConvert.SerializeObject(this, Formatting.Indented);
    }

    [DataContract]
    public class Data
    {
        [DataMember(Name = "company", EmitDefaultValue = false)]
        public Company Company { get; set; }

        [DataMember(Name = "sponsor", EmitDefaultValue = false)]
        public Person Sponsor { get; set; }

        [DataMember(Name = "responsible", EmitDefaultValue = false)]
        public Person Responsible { get; set; }

        [DataMember(Name = "terms_and_conditions_checkbox", EmitDefaultValue = false)]
        public bool TermsAndConditionsCheckbox { get; set; }

        [DataMember(Name = "training_coupon_code", EmitDefaultValue = false)]
        public string TrainingCouponCode { get; set; }

        [DataMember(Name = "user_profile", EmitDefaultValue = false)]
        public string UserProfile { get; set; }

        [DataMember(Name = "user_sub_profile", EmitDefaultValue = false)]
        public string UserSubProfile { get; set; }

        [DataMember(Name = "billing_address", EmitDefaultValue = false)]
        public string BillingAddress { get; set; }

        [DataMember(Name = "opca", EmitDefaultValue = false)]
        public Opca Opca { get; set; }

        [DataMember(Name = "optin", EmitDefaultValue = false)]
        public bool Optin { get; set; }

        [DataMember(Name = "other_address", EmitDefaultValue = false)]
        public Address OtherAddress { get; set; }

        [DataMember(Name = "buyer", EmitDefaultValue = false)]
        public Buyer Buyer { get; set; }

        [DataMember(Name = "price", EmitDefaultValue = false)]
        public string Price { get; set; }

        [DataMember(Name = "product", EmitDefaultValue = false)]
        public string Product { get; set; }

        [DataMember(Name = "trainees", EmitDefaultValue = false)]
        public List<Trainee> Trainees { get; set; } = new List<Trainee>();
    }

    [DataContract]
    public class Company
    {
        [DataMember(Name = "name", EmitDefaultValue = false)]
        public string Name { get; set; }

        [DataMember(Name = "number", EmitDefaultValue = false)]
        public string Number { get; set; }

        [DataMember(Name = "address", EmitDefaultValue = false)]
        public Address Address { get; set; }
    }

    [DataContract]
    public class Address
    {
        [DataMember(Name = "street", EmitDefaultValue = false)]
        public string Street { get; set; }

        [DataMember(Name = "street_2", EmitDefaultValue = false)]
        public string Street2 { get; set; }

        [DataMember(Name = "city", EmitDefaultValue = false)]
        public string City { get; set; }

        [DataMember(Name = "country", EmitDefaultValue = false)]
        public string Country { get; set; }

        [DataMember(Name = "postcode", EmitDefaultValue = false)]
        public string Postcode { get; set; }

        [DataMember(Name = "mailbox", EmitDefaultValue = false)]
        public string Mailbox { get; set; }

        [DataMember(Name = "state_province", EmitDefaultValue = false)]
        public string StateProvince { get; set; }
    }

    [DataContract]
    public class Opca
    {
        [DataMember(Name = "name", EmitDefaultValue = false)]
        public string Name { get; set; }

        [DataMember(Name = "used", EmitDefaultValue = false)]
        public bool Used { get; set; }

        [DataMember(Name = "address", EmitDefaultValue = false)]
        public Address Address { get; set; }
    }

    [DataContract]
    public class Buyer
    {
        [DataMember(Name = "address", EmitDefaultValue = false)]
        public Address Address { get; set; }

        [DataMember(Name = "person", EmitDefaultValue = false)]
        public Person Person { get; set; }

        [DataMember(Name = "options", EmitDefaultValue = false)]
        public Options Options { get; set; }
    }

    [DataContract]
    public class Person
    {
        [DataMember(Name = "title", EmitDefaultValue = false)]
        public string Title { get; set; }

        [DataMember(Name = "email_address", EmitDefaultValue = false)]
        public string EmailAddress { get; set; }

        [DataMember(Name = "first_name", EmitDefaultValue = false)]
        public string FirstName { get; set; }

        [DataMember(Name = "last_name", EmitDefaultValue = false)]
        public string LastName { get; set; }

        [DataMember(Name = "phone", EmitDefaultValue = false)]
        public string Phone { get; set; }

        [DataMember(Name = "position", EmitDefaultValue = false)]
        public string Position { get; set; }
    }

    [DataContract]
    public class Trainee
    {
        [DataMember(Name = "session", EmitDefaultValue = false)]
        public string Session { get; set; }

        [DataMember(Name = "is_used", EmitDefaultValue = false)]
        public bool Used { get; set; }

        [DataMember(Name = "person", EmitDefaultValue = false)]
        public Person Person { get; set; }

        [DataMember(Name = "address")]
        public Address Address { get; set; }

        [DataMember(Name = "manager", EmitDefaultValue = false)]
        public Person Manager { get; set; }

        [DataMember(Name = "options", EmitDefaultValue = false)]
        public Options Options { get; set; }
    }

    [DataContract]
    public class Options
    {
        [DataMember(Name = "cvrex", EmitDefaultValue = false)]
        public string Cvrex { get; set; }

        [DataMember(Name = "cvrex_session", EmitDefaultValue = false)]
        public string CvrexSession { get; set; }

        [DataMember(Name = "end", EmitDefaultValue = false)]
        public string End { get; set; }

        [DataMember(Name = "other", EmitDefaultValue = false)]
        public string Other { get; set; }

        [DataMember(Name = "other_option", EmitDefaultValue = false)]
        public string OtherOption { get; set; }

        [DataMember(Name = "start", EmitDefaultValue = false)]
        public string Start { get; set; }
    }
}