﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Threading.Tasks;
using Abilways.Catalogue;
using Abilways.Catalogue.Enums;
using Abilways.Catalogue.Models;
using FormCsvToJson.Models;
using ICSharpCode.SharpZipLib.GZip;
using ICSharpCode.SharpZipLib.Tar;

namespace FormCsvToJson
{
    class Program
    {
        #region PROPERTIES
        private static HttpClient EbasicHttpClient { get; } = new HttpClient();

        private static HttpClient EmailHttpClient { get; } = new HttpClient();

        private static Converter Converter { get; } = new Converter();

        /// <summary>
        /// Chemin actuel
        /// </summary>
        private static string CurrentPath { get; } = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

        /// <summary>
        /// Chemin du dossier pour le stockage des formulaires extraits
        /// </summary>
        private static string FormsPath { get; } = Path.Combine(Path.Combine(CurrentPath, "forms"), DateTime.Now.ToString("dd-MM-yyyy"));

        /// <summary>
        /// Le chemin de l'archive téléchargé
        /// </summary>
        private static string TarGzFile { get; } = Path.Combine(FormsPath, "application_form.tar.gz");

        private static SftpConnector Sftp { get; } = new SftpConnector("front01.efedomain.com", "abilways", "mbxRTMwLux5NfeL3");
        #endregion

        /// <summary>
        /// Envoie un email d'avertissement si une inscription est faites sur une session invalide
        /// </summary>
        /// <param name="form"></param>
        /// <param name="filecontent"></param>
        /// <param name="receiver"></param>
        /// <returns></returns>
        private static async Task<bool> SendWarningEmail(Models.New.Form form, string filecontent, string receiver)
        {
            const string endpoint = "SendEmail";
            Email email = new Email
            {
                Attachments = null,
                Header = null,
                LinksOnly = false,
                Message = $"L'inscription {form.WebformId} pour la session '{form.Data?.Trainees?.FirstOrDefault(x => !String.IsNullOrEmpty(x.Session))?.Session}' a soulevé une erreur\n",
                Receiver = receiver,
                Sender = "jsontoebasicc@abilways.com",
                Title = $"[WCF-JSON][ERREUR] Une inscription requiert votre attention ({DateTime.Now:dd-MM-yyyy hh:mm})"
            };

            if (!String.IsNullOrEmpty(filecontent))
                email.Message += $"[*] Contenu du fichier:====BEGIN FILE====\n{filecontent}\n====END FILE====\n\n";
            email.Message += $"[*] Dump JSON récupéré: ====BEGIN DUMP====\n{form.Dump()}\n====END DUMP====";
            var result = await EmailHttpClient.PostAsJsonAsync(endpoint, email);
            return result.IsSuccessStatusCode;
        }

        /// <summary>
        /// Envoie le formulaire sur le service Ebasicc360
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        private static async Task<Response> SendToEbasicc360(Models.New.Form form)
        {
            const string endpoint = "Form.svc/SaveRegistration";

            if (form == null)
                return null;
            var result = await EbasicHttpClient.PostAsJsonAsync(endpoint, form);
            if (result.IsSuccessStatusCode)
            {
                return await result.Content.ReadAsAsync<Response>();
            }
            return null;
        }

        /// <summary>
        /// Decompresse l'archive tar gz contenant les formulaires à envoyer
        /// </summary>
        private static void DecompressTar()
        {
            Stream inStream = File.OpenRead(TarGzFile);
            Stream gzipStream = new GZipInputStream(inStream);
            TarArchive tarArchive = TarArchive.CreateInputTarArchive(gzipStream);

            tarArchive.ExtractContents(FormsPath);
            tarArchive.Close();
            gzipStream.Close();
            inStream.Close();
        }

        private static void Main()
        {
            // Setup httpclient
            try
            {
                EbasicHttpClient.BaseAddress = new Uri(ConfigurationManager.AppSettings["ebasiccUrl"]);
                EmailHttpClient.BaseAddress = new Uri(ConfigurationManager.AppSettings["emailUrl"]);
            }
            catch (Exception e)
            {
                throw new ConfigurationErrorsException($"Merci de bien vouloir renseigner les clés 'ebasiccUrl' et 'emailUrl'\nException reçu : {e.Message}");
            }
            var json = new MediaTypeWithQualityHeaderValue("application/json");
            EbasicHttpClient.DefaultRequestHeaders.Accept.Add(json);
            // Tar gz download and extract
            Directory.CreateDirectory(FormsPath);
            DirectoryInfo dir = new DirectoryInfo(FormsPath);
            Sftp.DownloadFile("exports/abilways/application_form.tar.gz", FormsPath);
            if (!File.Exists(TarGzFile))
                throw new FileNotFoundException("Le tar.gz contenant les fichiers n'a pas pu être téléchargé. Abandon de l'opération");
            Console.WriteLine("Download OK\nExtracting content...");
            DecompressTar();
            FileInfo[] files = dir.GetFiles("*.json");
            Console.WriteLine($"{files.Length} to send...");
            using (var ctx = new CatalogueEntities())
            {
                using (StreamWriter logFileStream = new StreamWriter(Path.Combine(FormsPath, "logs.txt"), true))
                {
                    logFileStream.AutoFlush = true;
                    logFileStream.WriteLine($"---- START : {DateTime.Now} ----");
                    foreach (FileInfo file in files)
                    {
                        using (StreamReader stream = new StreamReader(file.FullName))
                        {
                            Console.WriteLine(file.Name);
                            string payload = stream.ReadToEnd();
                            try
                            {
                                Models.New.Form form = Converter.ToJsonForm(payload);
                                if (form != null)
                                {
                                    Session session = ctx.Sessions.Single(form.Data?.Trainees
                                        .FirstOrDefault(x => x.Session != null)?.Session);
                                    // La session existe et est Valide ou Ouverte
                                    if (session != null && (session.Status == LearningObjectStatus.Ouverte || session.Status == LearningObjectStatus.Validee))
                                    {
                                        // Check si le form est associé à une session ouverte
                                        Response response = SendToEbasicc360(form).Result;
                                        logFileStream.WriteLine(response != null
                                            ? $"[SUCCESS] [{DateTime.Now}] Form '{file.Name}' sent to Ebasicc360 (Id : {response.Id})"
                                            : $"[ERROR] [{DateTime.Now}] Failed to send '{file.Name}' to Ebasicc360");
                                    }
                                    else
                                    {
                                        string reason = session == null
                                            ? "Session doesn't exist"
                                            : $"Session {session.ExtId} has status {session.Status.ToString()}";
                                        bool emailSent = SendWarningEmail(form, payload, "luberti@abilways.com").Result;
                                        logFileStream.WriteLine($"[ERROR] Error while retrieving session | Warned user about it ? {emailSent} | Reason : {reason}");
                                    }
                                }
                                else
                                    logFileStream.WriteLine($"[INFO] Didn't send '{file.Name}' to Ebasicc360.");
                            }
                            catch (Exception ex)
                            {
                                logFileStream.WriteLine(
                                    $"[ERROR] [{DateTime.Now}] Failed to convert '{file.Name}' to specific JSON format.\nGot : {ex.Message}\n{ex.StackTrace}");
                            }
                        }
                    }
                    files.ToList().ForEach(x => File.Delete(x.FullName));
                    logFileStream.WriteLine($"---- END : {DateTime.Now} ----");
                }
            }
            File.Delete(TarGzFile);
        }
    }
}